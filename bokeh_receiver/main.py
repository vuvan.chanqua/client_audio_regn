# -*- coding: utf-8 -*-
#
# from bokeh.io import curdoc
# from bokeh.plotting import ColumnDataSource, figure
#
# import receiver
#
# FPS = 1
#
# doc = curdoc()
# source = ColumnDataSource({'x': [], 'y': []})
#
# f = figure()
# l = f.line(x='x', y='y', source=source)
#
#
# def update():
#     if receiver.buffer:
#         data = receiver.buffer[-1]
#         print(data)
#         source.data.update(x=data, y=data)
#
#
# doc.add_periodic_callback(update, 1000 / FPS)
# doc.add_root(f)
#


import json
import os

import numpy as np
import receiver
import widget
from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource
from bokeh.models import Div, FixedTicker, FuncTickFormatter, HoverTool, Slider
from bokeh.models.callbacks import CustomJS
from bokeh.models.mappers import LinearColorMapper
from bokeh.models.widgets import DataTable, TableColumn, Button
from bokeh.palettes import Viridis256
from bokeh.plotting import figure
from flask import redirect

from config import *

rootDir = os.path.dirname(os.path.abspath(__file__))

with open(rootDir + '/model_labels.json', 'r') as labels_file:
    labels = json.load(labels_file)

PALETTE = Viridis256
PALETTE_DEFAULT_THRESHOLD = 0.5
SPEC_PALETTE = Viridis256

WIDTHS = [500, 100]
HEIGHTS = [200, 50 + 11 * len(labels)]
GRID_COLOR = '#eefeed'
TEXT_COLOR = '#545565'
TEXT_FONT = 'Signika'
SLIDER_WIDTH = 250

threshold = Slider(start=0.0, end=1.0, value=PALETTE_DEFAULT_THRESHOLD, step=0.05,
                   callback_policy='mouseup', title='Ngưỡng Xác suất xuất hiện để detect',
                   width=SLIDER_WIDTH, css_classes=['threshold-slider'])

SPEC_WIDTH = np.shape(receiver.spectrogram)[1]
SPEC_HEIGHT = np.shape(receiver.spectrogram)[0]

DETECTION = ColumnDataSource(
    data=dict(
        pos=[],
        label=[],
        value=[],
        pretty_value=[],
        color=[],
    )
)

HISTORY = ColumnDataSource(
    data=dict(
        label=[],
        x=[],
        y=[],
        value=[],
        pretty_value=[],
        color=[],
    ),
)

HISTORY_LABEL = ColumnDataSource(
    data=dict(
        label=[],
        x=[],
        y=[],
        value=[],
        pretty_value=[],
        color=[],
    ),
)

THRESHOLD = ColumnDataSource(
    data=dict(
        threshold=[PALETTE_DEFAULT_THRESHOLD - 0.01]
    )
)

SPECTROGRAM = ColumnDataSource(
    data=dict(
        value=[np.zeros((SPEC_HEIGHT, SPEC_WIDTH), dtype='float32')]
    )
)

LIVE_AUDIO = ColumnDataSource(
    data=dict(
        signal=[],
    )
)


def to_percentage(number):
    """Convert a float fraction into percentage representation.
    E.g. 0.953212 -> 95.32
    """
    return int(number * 1000) / 10.


def colorizer(value, reverse=False, low=0., high=1., threshold=None):
    if threshold is not None and value < threshold:
        return '#fefffe'
    idx = (value - low) / (high - low) * len(PALETTE)
    idx = int(np.clip(idx, 0, len(PALETTE)))
    return PALETTE[idx] if not reverse else PALETTE[-(idx + 1)]


def plot_spectrogram():
    # Spectrogram image
    plt = figure(plot_width=WIDTHS[0], plot_height=HEIGHTS[0],
                 toolbar_location=None, tools="",
                 x_range=[0, SPEC_WIDTH],
                 y_range=[0, SPEC_HEIGHT])

    plt.image('value', x=0, y=0, dw=SPEC_WIDTH, dh=SPEC_HEIGHT, name='spectrogram',
              color_mapper=LinearColorMapper(SPEC_PALETTE, low=0, high=100), source=SPECTROGRAM)

    # X ticks
    plt.xaxis[0].ticker = FixedTicker(ticks=[])

    # X axis
    plt.xaxis.axis_line_color = None

    # Y ticks
    plt.yaxis[0].ticker = FixedTicker(ticks=[])
    plt.yaxis.major_label_text_font_size = '0pt'
    plt.yaxis.major_tick_line_color = None

    # Y axis
    plt.yaxis.axis_line_color = None
    plt.yaxis.axis_label = 'Mel bands'
    plt.yaxis.axis_label_text_font = TEXT_FONT
    plt.yaxis.axis_label_text_font_size = '9pt'
    plt.yaxis.axis_label_text_font_style = 'normal'

    # Plot fill/border
    plt.background_fill_color = GRID_COLOR
    plt.outline_line_color = GRID_COLOR
    plt.min_border = 10

    # Plot title
    # plt.title.text = 'Hình ảnh Quang phổ âm thanh thu từ mic:'
    plt.title.text = 'Hinh anh pho quang tu mic:'
    plt.title.align = 'left'
    plt.title.text_color = TEXT_COLOR
    plt.title.text_font = TEXT_FONT
    plt.title.text_font_size = '9pt'
    plt.title.text_font_style = 'normal'

    return plt


def plot_detection_history():
    # Rectangle grid with detection history
    cols = np.shape(receiver.predictions)[1]

    plt = figure(plot_width=WIDTHS[0], plot_height=HEIGHTS[1],
                 toolbar_location=None, tools="hover",
                 x_range=[-cols, 0], y_range=labels[::-1])

    plt.rect(x='x', y='y', width=0.95, height=0.8, color='color', source=HISTORY)

    # X ticks
    plt.xaxis[0].ticker = FixedTicker(ticks=np.arange(-cols, 1, 1).tolist())
    plt.xaxis[0].formatter = FuncTickFormatter(code="""
        return (tick * {} / 1000).toFixed(1) + " s"
    """.format(PREDICTION_STEP_IN_MS))
    plt.xaxis.major_tick_line_color = GRID_COLOR
    plt.xaxis.major_label_text_font_size = '7pt'
    plt.xaxis.major_label_text_font = TEXT_FONT
    plt.xaxis.major_label_text_color = TEXT_COLOR

    # X axis
    plt.xaxis.axis_line_color = None

    # Y ticks
    plt.yaxis.major_tick_line_color = None
    plt.yaxis.major_label_text_font_size = '7pt'
    plt.yaxis.major_label_text_font = TEXT_FONT
    plt.yaxis.major_label_text_color = TEXT_COLOR

    # Y axis
    plt.yaxis.axis_line_color = GRID_COLOR

    # Grid
    plt.ygrid.grid_line_color = None
    plt.xgrid.grid_line_color = None

    # Plot fill/border
    plt.background_fill_color = GRID_COLOR
    plt.outline_line_color = GRID_COLOR
    plt.min_border = 10

    # Plot title
    plt.title.text = 'Các Frame detect trước đó:'
    plt.title.align = 'left'
    plt.title.text_color = TEXT_COLOR
    plt.title.text_font = TEXT_FONT
    plt.title.text_font_size = '9pt'
    plt.title.text_font_style = 'normal'

    # Hover toolsác
    hover = plt.select(dict(type=HoverTool))
    hover.tooltips = [
        ("Nhãn", "@label"),
        ('Xác suất', '@pretty_value'),
    ]

    return plt


def plot_detection_last():
    # Horizontal bars with current probabilities
    plt = figure(plot_width=WIDTHS[1], plot_height=HEIGHTS[1],
                 toolbar_location=None, tools='hover',
                 x_range=[0., 1.], y_range=labels[::-1])

    plt.hbar(y='pos', height=0.9, left=0, right='value', color='color', source=DETECTION,
             name='bars', line_color=None)

    # Threshold annotation
    plt.quad(left=-0.1, right='threshold', bottom=-0.1, top=len(labels) + 1, source=THRESHOLD,
             fill_color='#000000', fill_alpha=0.1, line_color='red', line_dash='dashed')

    # X ticks
    plt.xaxis[0].ticker = FixedTicker(ticks=[0, 1])
    plt.xaxis.major_tick_line_color = GRID_COLOR
    plt.xaxis.major_label_text_font_size = '7pt'
    plt.xaxis.major_label_text_font = TEXT_FONT
    plt.xaxis.major_label_text_color = TEXT_COLOR

    # X axis
    plt.xaxis.axis_line_color = None

    # Y ticks
    plt.yaxis[0].ticker = FixedTicker(ticks=np.arange(1, len(labels) + 1, 1).tolist())
    plt.yaxis.major_label_text_font_size = '0pt'
    plt.yaxis.major_tick_line_color = None

    # Y axis
    plt.yaxis.axis_line_color = GRID_COLOR

    # Grid
    plt.xgrid.grid_line_color = None
    plt.ygrid.grid_line_color = None

    # Band fill
    plt.hbar(y=np.arange(1, len(labels) + 1, 2), height=1., left=0, right=1., color='#000000',
             alpha=0.1, level='image', name='bands')

    # Plot fill/border
    plt.outline_line_color = GRID_COLOR
    plt.min_border = 10

    # Plot title
    plt.title.text = 'Frame Hiện Tại:'
    plt.title.text_color = TEXT_COLOR
    plt.title.text_font = TEXT_FONT
    plt.title.text_font_size = '9pt'
    plt.title.text_font_style = 'normal'

    # Hover tools
    hover = plt.select(dict(type=HoverTool))
    hover.names = ['bars']
    hover.tooltips = [
        ('Nhãn', '@label'),
        ('Xác suất', '@pretty_value'),
    ]

    return plt


def datatable_display():
    print("list ip")
    print(widget.list_ip)
    list_ip = []
    list_user = []
    for i in widget.list_ip:
        list_ip.append(i)
    for i in range(len(widget.list_ip)):
        list_user.append("user")
    data = dict(
        users=list_user,
        ip=list_ip,
    )
    source = ColumnDataSource(data)

    # template = """
    #             <div style="background:<%=
    #                 (function colorfromint(){
    #                     if(cola > colb ){
    #                         return("green")}
    #                     }()) %>;
    #                 color: black">
    #             <%= value %>
    #             </div>
    #             """
    # formatter = HTMLTemplateFormatter(template=template)

    columns = [TableColumn(field="users", title="User", width=100),
               # TableColumn(field='colb', title='Ip', formatter=formatter, width=100)]
               TableColumn(field='ip', title='Ip', width=100)]

    data_table = DataTable(source=source,
                           columns=columns,
                           fit_columns=True,
                           selectable=True,
                           sortable=True,
                           width=400, height=400)

    def callback(attrname, old, new):
        selectionIndex = source.selected['1d']['indices']
        datatable_choose_row(selectionIndex)

    source.on_change('selected', callback)
    return data_table


def datatable_choose_row(index):
    ip = widget.list_ip[index[0]]

    button = Button(label="Back", button_type="success")

    grid = column(
        row(spec_plot),
        row(history_plot, last_plot),
        row(button)
    )
    button.on_click(back_to_datatable)

    curdoc().add_root(grid)
    # t = Thread(target=receiver.stream_setup(ip))
    # t.start()


def back_to_datatable():
    print("cc")
    return redirect("http://127.0.0.1:5000")


def history_label():
    # Rectangle grid with detection history
    cols = np.shape(receiver.predictions)[1]
    plt = figure(
        plot_width=WIDTHS[0],
        plot_height=HEIGHTS[1],
        toolbar_location=None,
        tools="hover",
        x_range=[-cols, 0],
        y_range=labels[::-1]
    )

    plt.rect(x='x', y='y', width=0.95, height=0.8, color='color', source=HISTORY_LABEL)

    # X ticks
    plt.xaxis[0].ticker = FixedTicker(ticks=np.arange(-cols, 1, 1).tolist())
    plt.xaxis[0].formatter = FuncTickFormatter(code="""
            return (tick * {} / 1000).toFixed(1) + " s"
        """.format(PREDICTION_STEP_IN_MS))
    plt.xaxis.major_tick_line_color = GRID_COLOR
    plt.xaxis.major_label_text_font_size = '7pt'
    plt.xaxis.major_label_text_font = TEXT_FONT
    plt.xaxis.major_label_text_color = TEXT_COLOR

    # X axis
    plt.xaxis.axis_line_color = None

    # Y ticks
    plt.yaxis.major_tick_line_color = None
    plt.yaxis.major_label_text_font_size = '7pt'
    plt.yaxis.major_label_text_font = TEXT_FONT
    plt.yaxis.major_label_text_color = TEXT_COLOR

    # Y axis
    plt.yaxis.axis_line_color = GRID_COLOR

    # Grid
    plt.ygrid.grid_line_color = None
    plt.xgrid.grid_line_color = None

    # Plot fill/border
    plt.background_fill_color = GRID_COLOR
    plt.outline_line_color = GRID_COLOR
    plt.min_border = 10

    # Plot title
    plt.title.text = 'Nhãn hiển thị:'
    plt.title.align = 'center'
    plt.title.text_color = TEXT_COLOR
    plt.title.text_font = TEXT_FONT
    plt.title.text_font_size = '9pt'
    plt.title.text_font_style = 'normal'

    # Hover tool
    hover = plt.select(dict(type=HoverTool))
    hover.tooltips = [
        ("Nhãn", "@label"),
        ('Xác suất', '@pretty_value'),
    ]

    return plt


def update():
    if len(receiver.live_audio_feed):
        spectrogram = receiver.spectrogram.copy()
        predictions = receiver.predictions.copy()

        rows = len(labels)
        cols = np.shape(predictions)[1]
        pred = predictions[:, -1].tolist()

        # Push new audio
        data = dict(
            signal=[receiver.live_audio_feed.pop()]
        )

        LIVE_AUDIO.data = data

        # Update spectrogram
        data = dict(
            value=[spectrogram],
        )

        SPECTROGRAM.data = data

        # Update last detection plot data
        DETECTION.data = dict(
            pos=np.arange(0, rows, 1)[::-1] + 1,  # in reversed order
            label=labels,
            value=pred,
            pretty_value=[str(to_percentage(v)) + '%' for v in pred],
            color=[colorizer(v, True, threshold=threshold.value) for v in pred]
        )

        # Update threshold line
        THRESHOLD.data = dict(
            threshold=[threshold.value - 0.01]
        )

        # Update detection history data
        data = dict(
            label=[],
            x=[],
            y=[],
            value=[],
            pretty_value=[],
            color=[],
        )

        data_label = dict(
            label=[],
            x=[],
            y=[],
            value=[],
            pretty_value=[],
            color=[],
        )

        for r in range(rows):
            for c in range(cols):
                data['label'].append(labels[r])
                data['x'].append(0.5 + c - cols)
                data['y'].append(rows - r)  # inverted order
                data['value'].append(predictions[r, c])
                data['pretty_value'].append(str(to_percentage(predictions[r, c])) + '%')
                data['color'].append(colorizer(predictions[r, c], True, threshold=threshold.value))

                data_label['label'].append(labels[r])
                data_label['x'].append(0.5 + c - cols)
                data_label['y'].append(rows - r)  # inverted order
                data_label['value'].append(predictions[r, c])
                data_label['pretty_value'].append(str(to_percentage(predictions[r, c])) + '%')
                data_label['color'].append(colorizer(predictions[r, c], True, threshold=threshold.value))

        HISTORY.data = data
        HISTORY_LABEL.data = data_label


live_audio_callback = CustomJS(args=dict(feed=LIVE_AUDIO), code="""
    if (cb_obj.attributes.name == 'mute_button') {{
        if (is_muted) {{
            is_muted = false;
            cb_obj.attributes.label = 'Mute';
            cb_obj.trigger('change');
            console.log('Playback unmuted');
        }} else {{
            is_muted = true;
            cb_obj.attributes.label = 'Unmute';
            cb_obj.trigger('change');
            console.log('Playback muted');
        }}
    }}
    var new_signal = feed.data.signal[0];
    var node = audio_context.createBufferSource();
    var buffer = audio_context.createBuffer(1, {}, {});
    var signal = buffer.getChannelData(0);
    var streaming_delay = {};
    if (next_chunk_time == 0) {{
        next_chunk_time = audio_context.currentTime + streaming_delay;
    }}
    if (!is_muted) {{
        for (var i=0; i<new_signal.length; i++) {{
            signal[i] = new_signal[i];
        }}
        node.buffer = buffer;
        node.connect(audio_context.destination);
        node.start(next_chunk_time);
    }}
    next_chunk_time = next_chunk_time + buffer.duration;
""".format(BLOCK_SIZE * PREDICTION_STEP, SAMPLING_RATE, PREDICTION_STEP_IN_MS / 5000.))

LIVE_AUDIO.js_on_change('data', live_audio_callback)

spec_plot = plot_spectrogram()
history_plot = plot_detection_history()
history_label = history_label()
last_plot = plot_detection_last()
data_table = datatable_display()

filler = Div(width=(np.sum(WIDTHS) - SLIDER_WIDTH) // 2)
filler2 = Div(width=(np.sum(WIDTHS) - SLIDER_WIDTH) // 2 + 20)
#
# back_button = Button(label="Back", button_type="success")
# back_button.on_click(back_to_datatable)

# def function_to_call(attr, old, new):
#     receiver.stream_setup(dropdown.value)

# dropdown button
# menu = []
# for r in widget.list_ip:
#     menu.append(
#         (r, r)
#     )
# menu.append(
#     ("192.168.31.251", "192.168.31.251")
# )
# dropdown = Dropdown(label="Select Ip", button_type="warning", menu=menu)
# dropdown.on_change('value', function_to_call)


grid = column(
    row(spec_plot),
    row(history_plot, last_plot),
    # row(history_label)
    # row(filler, dropdown)
    # row(filler, widgetbox(threshold)),
    # row(filler2, back_button)
)

# table_grid = column(
#     row(data_table),
#     # row(filler, widgetbox(threshold)),
#     # row(filler2, Button(label='Mute', callback=live_audio_callback, name='mute_button'))
#     sizing_mode='stretch_both')
curdoc().title = 'Nhận Dạng Âm Thanh Thời Gian Thực'
curdoc().add_root(grid)
curdoc().add_periodic_callback(update, 100)
